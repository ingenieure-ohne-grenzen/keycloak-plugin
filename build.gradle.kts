/*
 *    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import java.time.Instant

plugins {
    val kotlinVersion = "2.1.10"

    kotlin("jvm") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion
    id("com.github.johnrengelman.shadow") version "8.1.1"
    id("com.palantir.git-version") version "3.1.0"
    `maven-publish`
}

val gitVersion: groovy.lang.Closure<String> by extra
val versionString = gitVersion()

group = "dev.maximilian.feather.keycloak"
version = versionString

val semVerRegex =
    @Suppress("ktlint:standard:max-line-length")
    Regex(
        "^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?\$",
    )

val isCi: Boolean = System.getenv("CI") == "true"
val ciApiUrl: String? = System.getenv("CI_API_V4_URL")
val ciProjectId: String? = System.getenv("CI_PROJECT_ID")
val isPublishPossible = isCi && ciApiUrl != null && ciProjectId != null && semVerRegex.matches(versionString)

repositories {
    mavenCentral()
}

val keycloakVersion = "26.1.3"
val wsRsApiVersion = "4.0.0"
val ktorVersion = "3.1.1"
val junitVersion = "5.12.0"

val microUtilsVersion = "3.0.5"
val slf4jVersion = "2.0.17"

dependencies {
    // Dependencies to keycloak
    compileOnly("org.keycloak", "keycloak-server-spi", keycloakVersion)
    compileOnly("org.keycloak", "keycloak-server-spi-private", keycloakVersion)
    compileOnly("org.keycloak", "keycloak-core", keycloakVersion)
    compileOnly("jakarta.ws.rs", "jakarta.ws.rs-api", wsRsApiVersion)

    // Http REST Client
    implementation("io.ktor", "ktor-client-core", ktorVersion)
    implementation("io.ktor", "ktor-client-cio", ktorVersion)
    implementation("io.ktor", "ktor-client-logging", ktorVersion)
    implementation("io.ktor", "ktor-client-content-negotiation", ktorVersion)
    implementation("io.ktor", "ktor-serialization-kotlinx-json", ktorVersion)

    // Logging
    implementation("org.slf4j", "slf4j-api", slf4jVersion)
    implementation("io.github.microutils", "kotlin-logging", microUtilsVersion)

    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter", "junit-jupiter-api", junitVersion)
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", junitVersion)
}

tasks {
    compileJava {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }

    compileTestJava {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }

    compileKotlin {
        kotlinOptions.jvmTarget = "17"
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "17"
    }

    test {
        useJUnitPlatform()
    }

    jar {
        manifest {
            attributes(
                "Built-By" to System.getProperty("user.name"),
                "Build-Timestamp" to Instant.now(),
                "Created-By" to "Gradle ${gradle.gradleVersion}",
                "Build-Jdk" to "${System.getProperty("java.version")} (${System.getProperty("java.vendor")} ${
                    System.getProperty(
                        "java.vm.version",
                    )
                })",
                "Build-OS" to "${System.getProperty("os.name")} ${System.getProperty("os.arch")} ${System.getProperty("os.version")}",
                "Version" to "${gradle.rootProject.version}",
                "Target-Keycloak-Version" to keycloakVersion,
            )
        }
    }
}

if (isPublishPossible) {
    publishing {
        publications {
            shadow {
                create<MavenPublication>("maven") {
                    from(components["java"])
                }
            }
        }

        repositories {
            maven {
                url = uri("$ciApiUrl/projects/$ciProjectId/packages/maven")
                name = "GitLab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }
    }
}
