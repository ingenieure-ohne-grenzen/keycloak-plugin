# Feather Keycloak Plugin

This plugin checks at every login in KeyCloak whether there are open actions in [Feather](https://gitlab.com/ingenieure-ohne-grenzen/feather). If there are open actions, the login process is interrupted and the user get's redirected to feather.

## Project setup

Install OpenJDK 17 as a prerequisite to compile and use this Kotlin KeyCloak Plugin.

### Compile and build jar file

```[bash]
./gradlew clean shadowJar
```

### Run and test

Currently, there are no automatic tests.

#### New way

Use the "local" Feather setup from [here](https://gitlab.com/ingenieure-ohne-grenzen/feather/-/blob/main/integration/local/README.md).

```[bash]
# step 1: build artefact
./gradlew clean shadowJar

# step 2: copy artefact to feather "local" folder
 FEATHER_LOCAL_FOLDER=/path/to/git/repo/feather/integration/local
cp build/libs/feather-keycloak-plugin-*.jar $FEATHER_LOCAL_FOLDER/feather-keycloak-plugin.jar

# step 3: start the local environment
cd $FEATHER_LOCAL_FOLDER
./local_up.sh --keycloak=current --no-backend

# read the Keycloak admin password from the printed log messages
# KEYCLOAK_ADMIN_PASSWORD=xxx
# KEYCLOAK_ADMIN_PASSWORD=xxx
# your can open the Keycloak Admin Console at
# http://localhost:16015/admin/master/console/

# step 4: start the Feather backend
# your need the postgresql admin password from the logs above
#POSTGRES_FEATHER_PASSWORD=xxx
cd ../.. # go to main git repo folder
DATABASE_URL="jdbc:postgresql://localhost:16010/feather?user=feather&password=$POSTGRES_FEATHER_PASSWORD&ssl=false" /mnt/homepart/jan/git_repos/feather/gradlew run

# from these logs, read the new Feather admin credentials
#
#[main] WARN dev.maximilian.feather.Main - ########## NO ADMIN USER FOUND
#
#
#No admin user found, creating "admin <admin@example.org>" with password "xxx"
# go to http://localhost:16014/dashboard to log into the Feather frontend
```

Create other Feather users at your convenience.

#### Old way

To automatically setup a local KeyCloak with the plugin the bash scripts in the `integration` folder can be used. For example to quickly get the plugin up and running, exec the following command:

```[bash]
./gradlew clean shadowJar && ./integration/local_up.sh
```

This requires that Feather is also deployed [locally](https://gitlab.com/ingenieure-ohne-grenzen/feather/-/blob/main/integration/README.md#minimal-deployment).

For better testing, set up an LDAP federation:

```[bash]
docker-compose --project-directory "./integration" exec keycloak /ldap_federation.sh $LDAP_HOST $LDAP_BASE_DN_USERS $LDAP_BIND_DN
```

Note that this setup needs several additional steps such as the configuration of the Feather Client in Keycloak.

#### Work with an existing LDAP (e.g. DEV cloud)

In this last example, we use an existing LDAP instance e.g. from our DEV cluster so that we already a number of test users prepared.

```[bash]
# step 1: build and start integration instance
./gradlew clean shadowJar && ./integration/local_up.sh

# now you can log into Keycloak Admin console at
# http://localhost:8080/admin/master/console/
# with credentials admin:admin

# step 2: configure LDAP federation
LDAP_HOST=ipa.typeerror.eu
LDAP_BASE_DN_USERS="cn=accounts,dc=typeerror,dc=eu"
LDAP_BIND_DN="cn=Directory Manager"
docker-compose --project-directory "./integration" exec keycloak /ldap_federation.sh $LDAP_HOST "cn=users,$LDAP_BASE_DN_USERS" "$LDAP_BIND_DN"
docker-compose --project-directory "./integration" exec keycloak /migrate.sh
docker-compose --project-directory "./integration" exec keycloak /migrate_feather.sh "cn=groups,$LDAP_BASE_DN_USERS"
# remember the client secret printed in the logs of the last command

# after this step you can import the LDAP uses into Keycloak at once
# (this will also happen automatically step-by-step for each user that logs into)
```

Now, turn to the [Feather minimal deployment](https://gitlab.com/ingenieure-ohne-grenzen/feather/-/blob/main/integration/README.md#minimal-deployment) to start a local Feather development instance which uses the Keycloak that you just configured.

```[bash]
cd /path/to/git/repo/feather/integration/minimal_deploy
docker-compose up -d

# step 3: Start feather for a first time to initialize the PSQL table
cd ../..
export DATABASE_URL="jdbc:postgresql://localhost:5432/postgres?user=postgres&password=postgres"
./gradlew run
# This start will fail with exception "LDAP base dn must be not blank".
# But afterwards, the properties table is initialized.

# Step 4: Complete the initialization of the background services.
cd ./integration/minimal_deploy
./set_ldap_properties.sh $LDAP_HOST "$LDAP_BASE_DN_USERS" "$LDAP_BIND_DN" 636 cloud
psql -h localhost -p 5432 --user postgres
# enter password 'postgres'
> INSERT INTO properties (key, value) VALUES ('baseurl', 'http://localhost:3000');

# Step 5: Re-start feather backend.
cd ../..
./gradlew run
# This starts the backend on port 7000.

# Step 6: Start a development feather frontend.
cd /path/to/git/repo/featherFrontend
VITE_API_BASE_URL=http://localhost:7000 yarn serve
# This starts the frontend on port 3000.
```

Go to <http://localhost:3000/login> and start playing around.

### Lints and fixes files

Download 'ktlint' from <https://github.com/pinterest/ktlint/releases/latest/download/ktlint> if not yet done.

```[bash]
ktlint -F
```

**Install the pre-commit-hook** to mitigate commits with message `ktlint -F`. This is done by `ktlint installGitPreCommitHook`.

## License

Licensed under [Apache 2.0](LICENSE)
