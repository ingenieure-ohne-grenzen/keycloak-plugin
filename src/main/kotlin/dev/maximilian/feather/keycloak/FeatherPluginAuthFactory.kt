/*
 *    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.keycloak

import org.keycloak.Config
import org.keycloak.authentication.Authenticator
import org.keycloak.authentication.AuthenticatorFactory
import org.keycloak.models.AuthenticationExecutionModel
import org.keycloak.models.KeycloakSession
import org.keycloak.models.KeycloakSessionFactory
import org.keycloak.models.RoleModel
import org.keycloak.provider.ProviderConfigProperty
import org.keycloak.provider.ServerInfoAwareProviderFactory
import java.util.jar.JarFile
import kotlin.io.path.toPath
import kotlin.streams.toList

class FeatherPluginAuthFactory :
    AuthenticatorFactory,
    ServerInfoAwareProviderFactory {
    private val debug: Boolean = System.getenv("DEBUG") == "true"

    private val featherApiToken: String = System.getenv("FEATHER_API_TOKEN") ?: ""

    private val featherApiBaseUrl =
        requireNotNull(System.getenv("FEATHER_API_BASE_URL")) {
            "Feather Plugin activated, but no feather api base url supplied"
        }

    private val featherFrontendBaseUrl =
        requireNotNull(System.getenv("FEATHER_FRONTEND_BASE_URL")) {
            "Feather Plugin activated, but no feather frontend base url supplied"
        }

    private val featherAdminRole =
        requireNotNull(System.getenv("FEATHER_ADMIN_ROLE")) {
            "Feather Plugin activated, but no feather admin role supplied"
        }

    private val manifestInfo: MutableMap<String, String> by lazy {
        JarFile(
            this::class.java.protectionDomain.codeSource.location
                .toURI()
                .toPath()
                .toFile(),
        ).manifest
            .mainAttributes
            .map { it.key.toString() to it.value.toString() }
            .toMap()
            .toMutableMap()
    }

    private val requirementChoices =
        arrayOf(AuthenticationExecutionModel.Requirement.REQUIRED, AuthenticationExecutionModel.Requirement.DISABLED)

    private fun createAuthenticator(adminRole: RoleModel) =
        FeatherPluginAuthenticator(debug, featherApiToken, featherApiBaseUrl, featherFrontendBaseUrl, adminRole)

    override fun create(session: KeycloakSession): Authenticator {
        val adminRole: RoleModel? =
            session
                .roles()
                .getRealmRolesStream(session.context.realm)
                .toList()
                .firstOrNull { it.name == featherAdminRole }
        requireNotNull(adminRole) { "The specified admin role \"$featherAdminRole\" cannot be found in the roles keycloak supplies" }
        return createAuthenticator(adminRole)
    }

    override fun init(config: Config.Scope?) {}

    override fun postInit(factory: KeycloakSessionFactory?) {}

    override fun close() {}

    override fun getId(): String = "feather-keycloak-plugin"

    override fun getHelpText(): String = "Pls check documentation at https://gitlab.com/ingenieure-ohne-grenzen/keycloak-plugin"

    override fun getConfigProperties(): MutableList<ProviderConfigProperty> = mutableListOf()

    override fun getDisplayType(): String = "Feather KeyCloak Plugin"

    override fun getReferenceCategory(): String? = null

    override fun isConfigurable(): Boolean = false

    override fun getRequirementChoices(): Array<AuthenticationExecutionModel.Requirement> = requirementChoices

    override fun isUserSetupAllowed(): Boolean = false

    override fun getOperationalInfo(): MutableMap<String, String> = manifestInfo
}
