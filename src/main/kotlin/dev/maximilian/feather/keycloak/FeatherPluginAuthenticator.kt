/*
 *    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.keycloak

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import jakarta.ws.rs.core.Response
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable
import mu.KotlinLogging
import org.keycloak.authentication.AuthenticationFlowContext
import org.keycloak.authentication.Authenticator
import org.keycloak.models.KeycloakSession
import org.keycloak.models.RealmModel
import org.keycloak.models.RoleModel
import org.keycloak.models.UserModel
import java.net.URI

class FeatherPluginAuthenticator(
    private val debug: Boolean,
    private val featherApiToken: String,
    private val featherApiBaseUrl: String,
    private val featherFrontendBaseUrl: String,
    private val adminRole: RoleModel,
) : Authenticator {
    private val logger = KotlinLogging.logger { }

    override fun close() {}

    @Serializable
    private data class BackUrlBody(
        val backUrl: String,
    )

    private fun getUserToken(
        userId: String,
        backUrl: String,
    ): TokenAnswer =
        runBlocking {
            createHttpClient().use {
                it
                    .post("$featherApiBaseUrl/v1/plugins/keycloak-actions/check/$userId") {
                        setBody(BackUrlBody(backUrl))
                    }.body()
            }
        }

    override fun authenticate(context: AuthenticationFlowContext) {
        val user: UserModel = context.user
        val result =
            kotlin.runCatching {
                getUserToken(
                    user.getFirstAttribute("LDAP_ID") ?: user.id,
                    context.getRefreshUrl(false).toString(),
                )
            }

        result.onFailure {
            // In case of error is admin always allowed to log in
            if (user.hasRole(adminRole)) {
                logger.warn(it) { "Failure while crawling for user actions, but user is admin and should succeed" }
                context.success()
            } else {
                throw it
            }
        }

        result.onSuccess {
            if (it.token == null) {
                context.success()
            } else {
                context.forceChallenge(
                    Response
                        .seeOther(URI("$featherFrontendBaseUrl/?provider=keycloak-actions-plugin&code=${it.token}"))
                        .build(),
                )
            }
        }
    }

    override fun action(context: AuthenticationFlowContext?) {
        // noop
    }

    override fun requiresUser(): Boolean = true

    override fun configuredFor(
        session: KeycloakSession,
        realm: RealmModel,
        user: UserModel,
    ): Boolean = true

    override fun setRequiredActions(
        session: KeycloakSession?,
        realm: RealmModel?,
        user: UserModel?,
    ) {
        // noop
    }

    @Serializable
    private data class TokenAnswer(
        val token: String?,
    )

    private fun HttpClientConfig<*>.configureLogging() =
        Logging {
            logger =
                object : Logger {
                    override fun log(message: String) {
                        this@FeatherPluginAuthenticator.logger.info { message }
                    }
                }
            level = if (debug) LogLevel.ALL else LogLevel.INFO
        }

    private fun HttpClientConfig<*>.configureJson() =
        install(ContentNegotiation) {
            json(
                kotlinx.serialization.json.Json {
                    ignoreUnknownKeys = true
                },
            )
        }

    private fun createHttpClient(): HttpClient =
        HttpClient {
            configureLogging()
            configureJson()

            defaultRequest {
                contentType(ContentType.Application.Json)
                header("X-Token", featherApiToken)
            }
        }
}
