#!/bin/sh
#
#    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e
CHECK_URL="${CHECK_URL:-http://127.0.0.1:8080}"

keycloak_ready() {
  curl --fail -s -o /dev/null "$CHECK_URL" || false
  return $?
}

set +e
keycloak_ready
RESULT=$?
WAITED=0
while [ $RESULT -ne 0 ]; do
  sleep 5
  keycloak_ready
  RESULT=$?
  WAITED=$((WAITED+5))
  echo "Waited $WAITED seconds on KeyCloak to be ready"
done;
set -e
echo "KeyCloak ready"
