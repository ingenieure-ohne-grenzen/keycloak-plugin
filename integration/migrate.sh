#!/bin/sh
#
#    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

/opt/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/ --realm master --user admin --password admin
/opt/keycloak/bin/kcadm.sh create authentication/flows/browser/copy -s 'newName=Feather-Keycloak-Plugin'
EXECUTION_ID=$(/opt/keycloak/bin/kcadm.sh create -i authentication/flows/Feather-Keycloak-Plugin%20forms/executions/execution -s 'provider=feather-keycloak-plugin')
/opt/keycloak/bin/kcadm.sh update authentication/flows/Feather-Keycloak-Plugin/executions -s "id=$EXECUTION_ID" -s 'requirement=REQUIRED' --no-merge
/opt/keycloak/bin/kcadm.sh update realms/master -s 'browserFlow=Feather-Keycloak-Plugin'
