#!/bin/sh
#
#    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

LDAP_BASE_DN_GROUPS="$1"
FRONTEND_PORT="3000"

/opt/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/ --realm master --user admin --password admin
PARENT_ID=$(/opt/keycloak/bin/kcadm.sh create -r master -i clients -s clientId=feather -s "redirectUris=[\"http://localhost:$FRONTEND_PORT/*\"]")
/opt/keycloak/bin/kcadm.sh create -r master clients/$PARENT_ID/client-secret
CLIENT_SECRET=$(/opt/keycloak/bin/kcadm.sh get -r master clients/$PARENT_ID/client-secret --fields value --format csv --noquotes)

/opt/keycloak/bin/kcadm.sh create -r master clients/$PARENT_ID/protocol-mappers/models \
  -s name=uuid \
  -s protocol=openid-connect \
  -s protocolMapper=oidc-usermodel-attribute-mapper \
  -s 'config."user.attribute"="LDAP_ID"' \
  -s 'config."claim.name"="uuid"' \
  -s 'config."userinfo.token.claim"="true"' \
  -s 'config."id.token.claim"="true"' \
  -s 'config."access.token.claim"="true"' \
  -s 'config."jsonType.label"="String"'

/opt/keycloak/bin/kcadm.sh create -r master roles -s name=admin

/opt/keycloak/bin/kcadm.sh create components -r master \
    -s name=admin-role \
    -s providerId=role-ldap-mapper \
    -s providerType=org.keycloak.storage.ldap.mappers.LDAPStorageMapper \
    -s parentId=$PARENT_ID \
    -s 'config."membership.attribute.type"=["DN"]' \
    -s "config.\"roles.dn\"=[\"$LDAP_BASE_DN_GROUPS\"]" \
    -s 'config."roles.ldap.filter"=["(cn=admins)"]' \
    -s 'config."mode"=["READ_ONLY"]' \
    -s 'config."use.realm.roles.mapping"=["true"]'

echo $CLIENT_SECRET
