#!/bin/sh
#
#    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

usage() {
  echo "usage: ./ldap_federation.sh \$LDAP_HOST \$LDAP_BASE_DN_USERS \$LDAP_BIND_DN [\$LDAP_PROTOCOL]" >&2
}

if [ -z "$1" ]; then
  usage
  echo "Error: LDAP host is missing." >&2
  exit 1
fi
if [ -z "$2" ]; then
  usage
  echo "Error: LDAP base DN is missing." >&2
  exit 1
fi
if [ -z "$3" ]; then
  usage
  echo "Error: LDAP bind DN is missing." >&2
  exit 1
fi
LDAP_HOST="$1"
LDAP_BASE_DN_USERS="$2"
LDAP_BIND_DN="$3"
LDAP_PROTOCOL="${4:-ldaps}"

# Thanks to https://unix.stackexchange.com/a/223000
read_password() {
  REPLY="$(
    # always read from the tty even when redirected:
    exec < /dev/tty || exit # || exit only needed for bash

    # save current tty settings:
    tty_settings=$(stty -g) || exit

    # schedule restore of the settings on exit of that subshell
    # or on receiving SIGINT or SIGTERM:
    trap 'stty "$tty_settings"' EXIT INT TERM

    # disable terminal local echo
    stty -echo || exit

    # prompt on tty
    printf "Password: " > /dev/tty

    # read password as one line, record exit status
    IFS= read -r password; ret=$?

    # display a newline to visually acknowledge the entered password
    echo > /dev/tty

    # return the password for $REPLY
    printf '%s\n' "$password"
    exit "$ret"
  )"
}

read_password

if [ -z "$REPLY" ]; then
    echo "Password must be provided"
    exit 1
fi

LDAP_BIND_PW="$REPLY"

/opt/keycloak/bin/kcadm.sh create components -r master -i \
    -s name="test-ldap" \
    -s providerId="ldap" \
    -s providerType="org.keycloak.storage.UserStorageProvider" \
    -s 'config.editMode=["READ_ONLY"]' \
    -s 'config.priority=["1"]' \
    -s 'config.vendor=["rhds"]' \
    -s 'config.batchSizeForSync=["1000"]' \
    -s 'config.usernameLDAPAttribute=["uid"]' \
    -s 'config.rdnLDAPAttribute=["uid"]' \
    -s 'config.uuidLDAPAttribute=["nsuniqueid"]' \
    -s 'config.userObjectClasses=["inetOrgPerson, organizationalPerson"]' \
    -s "config.connectionUrl=[\"$LDAP_PROTOCOL://$LDAP_HOST\"]" \
    -s "config.usersDn=[\"$LDAP_BASE_DN_USERS\"]" \
    -s 'config.authType=["simple"]' \
    -s "config.bindDn=[\"$LDAP_BIND_DN\"]" \
    -s "config.bindCredential=[\"$LDAP_BIND_PW\"]" \
    -s "config.searchScope=[\"1\"]" \
    -s "config.useTruststoreSpi=[\"ldapsOnly\"]" \
    -s 'config.fullSyncPeriod=["-1"]' \
    -s 'config.changedSyncPeriod=["-1"]'
